"	standard stuff

syntax enable
set number relativenumber
set termguicolors
set noshowmode
set noswapfile
set clipboard=unnamedplus
set autoindent
set shiftwidth=4
set t_Co=256
set laststatus=2
set cursorline
hi Cursorline cterm=NONE ctermbg=grey ctermfg=black guibg=grey guifg=white
set mouse=nicr
set splitbelow splitright

let g:mapleader = "\<Space>"
let g:lightline = {
    \'colorscheme': 'material'
    \}

let g:indentLine_setColors = 0
let g:indentLine_char = '|'

"	Plugins

call plug#begin()

Plug 'itchyny/lightline.vim'
Plug 'neoclide/coc-highlight'
Plug 'jiangmiao/auto-pairs'
Plug 'wadackel/vim-dogrun'
Plug 'scrooloose/NERDTree'
Plug 'Yggdroot/indentLine'

call plug#end()


colorscheme dogrun

